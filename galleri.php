<?php session_start();
include("library.php");

$loggedIn = false;

connectToServer("projekt_backsjon", "password","localhost", "projekt_backsjon");	//connecting to server

$galleryData = getAllData("gallery");		//getting all galleryitems
$galleryAssoc = array();

while($row = mysqli_fetch_assoc($galleryData)){
	$galleryAssoc[] = $row;
}

if(checkIfLoggedIn()){
	$loggedIn = true;
}

 ?>


<!DOCTYPE html>


<html lang="sv">
<head>
<meta charset="utf-8">
<title>Projektet Bäcksjön</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link rel="stylesheet" href="css/style.css">
<script type="text/javascript" src="javascript/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="javascript/script.js"></script>
</head>

<body>
<div id="meny"></div>


<div id="mainContent">
	<div id="fileuploadDiv">
	</div>
	<p id="fileFormStatus"></p>		<!-- this needs to be outside of the the fileuploaddiv for some reason, maybe because load will overwrite shit -->
<?php

for($i = 0; $i < count($galleryAssoc); $i++){
	$data = $galleryAssoc[$i];
	echo "<div class='imgContainer'>";
	echo "<a href='img/".$data['image']."'><img class='image galleryImage' src='img/".$data['image']."' alt='".$data['caption']."'></a>";		//make this an <a>, to show larger version
	echo "<p>".$data['text']."</p>";
	echo "</div>";
}

?>

</div>

<div id="foot">
<p id="status"></p>
<p id="logout">Logga ut</p>
<div id="footContent"></div>
</div>

<?php 


function reload(){
	
	echo '
<script type="text/javascript">
location.href="galleri.php";
</script>';
}

function uploadFile(){
	
	echo "<script>showFileForm()</script>";
	
	if(isset($_POST["text"])){
		$uploadok= 1;
		$statusmess = "";
		
		$directory = "img/";		//directory to use
		$target_file = $directory .  basename($_FILES["file"]["name"]);	//directory + filename to use. dont trust "name" in a public situation
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);		//grabbing extension
		
		if (file_exists($target_file)){
			
			$result = query("SELECT * FROM gallery WHERE image='".basename($_FILES["file"]["name"])."'");
			
			if(mysqli_num_rows($result) == 0){
				if (query("INSERT INTO gallery (image,text) VALUES ('".$_FILES["file"]["name"]."','".cleanUp($_POST["text"])."')")){	//dont trust "name" in public..
					reload();
					return;
				}
				else{
					$uploadok = 0;
					$statusmess = "Det gick inte att kontakta servern";
					echo "<script>fileUploadMess('".$statusmess."')</script>";
				
				}
			}
			else{
				$uploadok = 0;
				$statusmess = "Det finns redan en fil med det namnet på servern";		
			}
				
			
	
		}
		
		if ($_FILES["file"]["size"] >  5 * 1024 * 1024){
			$uploadok = 0;
			$statusmess = "Filen är för stor. Max 5 mb tillåtet";
		}
		
		if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif"){
			
			$uploadok = 0;
			$statusmess = "Fel: Filen är varken jpg/jpeg/gif eller png";
		}
		
		//checking if success or not
		if ($uploadok == 1){
			
			if (query("INSERT INTO gallery (image,text) VALUES ('".$_FILES["file"]["name"]."','".cleanUp($_POST["text"])."')")){	//dont trust "name" in public..
			move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
			//$statusmess = "filen är uppladdad";
			//echo "<script>fileUploadMess('".$statusmess."')</script>";
			reload();
			}
			else{
				$statusmess = "Det gick inte att kontakta servern";
				echo "<script>fileUploadMess('".$statusmess."')</script>";
				
			}
			
		}
		else if ($uploadok == 0){	//fail
			echo "<script>fileUploadMess('".$statusmess."')</script>";
		};
		//clearFormVariables();
	}
}

if (!$loggedIn){

	if(checkInput("user") && checkInput("pass")){		//checking if logging in...
		if (loginUser($_POST["user"], $_POST["pass"])){
			echo("<script>statusChangeLogIn('".cleanUp($_POST["user"])."')</script>");		//success
			echo "<script>hideLogin();</script>";	//hiding login-field
			$_SESSION["user"] = $_POST["user"];
			$_SESSION["pass"] = $_POST["pass"];
			uploadFile();
		}
		else{
			echo("<script>statusChangeFail();</script>");			//fail
		}
	}
}
else{
	echo("<script>statusChangeLogIn('".cleanUp($_SESSION["user"])."')</script>");		//name
	echo "<script>hideLogin();</script>";	//hiding login-field
	uploadFile();
}

?>


</body>

</html>