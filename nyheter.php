<?php session_start();
include("library.php");

define("STD_IMG","standardbild.jpg");		//default image to be used when not selecting any image for newsposts

$loggedIn = false;

connectToServer("projekt_backsjon", "password","localhost", "projekt_backsjon");	//connecting to server

if(checkIfLoggedIn()){
	$loggedIn = true;
}

$data = getAllData("nyheter");
$dataArray = array();

while($row = mysqli_fetch_assoc($data)){
	$dataArray[] = $row;
}

$dataArray=array_reverse($dataArray);

 ?>


<!DOCTYPE html>


<html lang="sv">
<head>
<meta charset="utf-8">
<title>Projektet Bäcksjön</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link rel="stylesheet" href="css/style.css">
<script type="text/javascript" src="javascript/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="javascript/script.js"></script>
</head>

<body>
<div id="meny"></div>


<div id="mainContent">
	<div id="newsuploadDiv">
	</div>
	<p id="newsFormStatus"></p>		<!-- this needs to be outside of the the newsuploadDiv for some reason, maybe because load will overwrite shit -->


<?php
for($i = 0; $i < count($dataArray); $i++){
	if($i  == 5){
		break;
	}
	
	echo "<div class='newsDiv'>";
	echo "<img class='image newsImage' src='img/".$dataArray[$i]['image']."'>"; //bild
	echo "<h2>".$dataArray[$i]['titel']."</h2>"; 	//title
	echo "<strong><p>".$dataArray[$i]['datum']."</p></strong>";	//datum
	echo "<p>".$dataArray[$i]['text']."</p>";	//text
	
	echo "</div>";
}
?>

</div>

<div id="foot">
<p id="status"></p>
<p id="logout">Logga ut</p>
<div id="footContent"></div>
</div>

<?php 


function reload(){
	echo '
<script type="text/javascript">
location.href="nyheter.php";
</script>';
	
}

function uploadNews(){
	
	echo "<script>showNewsForm()</script>";
	
	if(isset($_POST["textnews"]) && isset($_POST["title"])){
		
		if($_FILES['filenews']['error'] != UPLOAD_ERR_NO_FILE){		//this newspost has a file given
			
			$uploadok= 1;
			$statusmess = "";
			
			$directory = "img/";		//directory to use
			$target_file = $directory .  basename($_FILES["filenews"]["name"]);	//directory + filename to use. dont trust "name" in a public situation
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);		//grabbing extension
		
			
			if ($_FILES["filenews"]["size"] >  5 * 1024 * 1024){
				$uploadok = 0;
				$statusmess = "Filen är för stor. Max 5 mb tillåtet";
			}
			
			if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif"){
				
				$uploadok = 0;
				$statusmess = "Fel: Filen är varken jpg/jpeg/gif eller png";
			}
			
			//checking if success or not
			if ($uploadok == 1){
				
				if (file_exists($target_file)){	//file exists, saving reference of it
					if (query("INSERT INTO nyheter (text,datum,titel,image) VALUES ('".$_POST["textnews"]."','".date('Y-m-d')."','".$_POST["title"]."','".
					$_FILES["filenews"]["name"]."')")){
						//$statusmess = "filen är uppladdad";
						//echo "<script>newsUploadMess('".$statusmess."')</script>";
						reload();
					}
					else{
						$statusmess = "Det gick inte att kontakta servern";
						echo "<script>newsUploadMess('".$statusmess."')</script>";
						
					}
				}
				
				else{		//file does not exist, moving it to server
					if (query("INSERT INTO nyheter (text,datum,titel,image) VALUES ('".$_POST["textnews"]."','".date('Y-m-d')."','".$_POST["title"]."','".
					$_FILES["filenews"]["name"]."')")){
						move_uploaded_file($_FILES["filenews"]["tmp_name"], $target_file);
						//$statusmess = "filen är uppladdad";
						//echo "<script>newsUploadMess('".$statusmess."')</script>";
						reload();
					}
					else{
						$statusmess = "Det gick inte att kontakta servern";
						echo "<script>newsUploadMess('".$statusmess."')</script>";
						
					}

				}
				
				
			}
			else if ($uploadok == 0){	//fail
				echo "<script>newsUploadMess('".$statusmess."')</script>";
			};
		}
		else{		//no image given, using standard image
			if (query("INSERT INTO nyheter (text,datum,titel,image) VALUES ('".$_POST["textnews"]."','".date('Y-m-d')."','".$_POST["title"]."','".STD_IMG."')")){	
			//$statusmess = "nyheten är uppladdad";
			//echo "<script>newsUploadMess('".$statusmess."')</script>";
			reload();
			}
			else{
				$statusmess = "Det gick inte att kontakta servern";
				echo "<script>newsUploadMess('".$statusmess."')</script>";
				
			}
		}
		//clearFormVariables();
	}
}



if (!$loggedIn){

	if(checkInput("user") && checkInput("pass")){		//checking if logging in...
		if (loginUser($_POST["user"], $_POST["pass"])){
			echo("<script>statusChangeLogIn('".cleanUp($_POST["user"])."')</script>");		//success
			echo "<script>hideLogin();</script>";	//hiding login-field
			$_SESSION["user"] = $_POST["user"];
			$_SESSION["pass"] = $_POST["pass"];
			uploadNews();
		}
		else{
			echo("<script>statusChangeFail();</script>");			//fail
		}
	}
}
else{
	echo("<script>statusChangeLogIn('".cleanUp($_SESSION["user"])."')</script>");		//name
	echo "<script>hideLogin();</script>";	//hiding login-field
	uploadNews();
}

?>


</body>

</html>