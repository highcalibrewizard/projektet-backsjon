<?php

/*
Library methods for connection, checking of illegal chars etc
include with
include("library.php");
*/

$connection = false;	//connection variable


/*
Cleaning up string data. do this on stuff that needs to be printed, like form data. otherwise you can inject javascript when the text is printed with php
*/
function cleanUp($stringData){
	global $connection;
	$stringData = htmlspecialchars($stringData); //removes 
	$stringData = trim($stringData); //removed spaces
	$stringData = stripslashes($stringData);	//removes slashes
	$stringData = mysqli_real_escape_string($connection,$stringData);	//removes escape strings to prevent sql injections
	return $stringData;
}


/*
this checks if $_SESSION["user"] and $_SESSION["pass"] exists. These vars should only be set if password checks out, no such check is here.
also it generates a new session_id to prevent exploits
*/
function checkIfLoggedIn(){
	
	if(!isset($_SESSION["initiated"])){	//preventing session fixation
		$_SESSION["initiated"] = 1;
		session_regenerate_id();
	}
	
	if(isset($_SESSION["user"]) && isset($_SESSION["pass"])){
		
		return true;
	}
	
	return false;
	
}


/*
Logging in and checking if supplied pass equals hashed pass
this uses a table you need to fill in yourself
it uses columns hash and user
returns false if failure
*/
function loginUser($user, $pass){
	global $connection;
	
	$pass = cleanUp($pass);	
	
	$result = null;
	
	$statement = mysqli_stmt_init($connection);
	
	if (mysqli_stmt_prepare($statement, "SELECT hash FROM users WHERE user=?")){
		mysqli_stmt_bind_param($statement, "s", $user);
		mysqli_stmt_execute($statement);
		mysqli_stmt_bind_result($statement, $result);
		mysqli_stmt_fetch($statement);
		mysqli_stmt_close($statement);
	}
	else{
		return false;
	}
	
	
	
	if(!$result){
		return false;
	}
	
	if(password_verify($pass,$result)){		//password verify will check between hashed password and the supplied password by user
		return true;
	}
	else{
		return false;
	}
}


/*
Generating a new user. needs too check if user exists and generate a password hash that is saved (column size of hash should be 255)
uses columns hash and user.
returns false if failure

!!!You will need to change the table name manually, its not possible in prepared statements!!!
*/
function makeNewUser($newUserName, $password){
	global $connection;
	
	//password is not cleaned up here, you need to do it in non-library code
	
	$result = null;
	
	$statement = mysqli_stmt_init($connection);
	if(mysqli_stmt_prepare($statement, 'SELECT user FROM users WHERE user=?')){
		mysqli_stmt_bind_param($statement, "s",$newUserName);		//binding two string params to the statement
		mysqli_stmt_execute($statement);		//executes statement
		mysqli_stmt_bind_result($statement, $result);		//declares that values should be saved in variable $result. you will need to do this in each column
		mysqli_stmt_fetch($statement);						//fetches values to $result. it wont be saved in array, so you have to do like below
		/*
		while($row = mysqli_stmt_fetch($statement)){
			print $row;
			//save into an array here
		}
		*/
		mysqli_stmt_close($statement);						//closes current statement
	}
	else{
		print "<script>console.log('prepared statement failed')</script>";
		return false;
	}
	
	if(!$result){		//if null or false no such data, so we can make new user
		
		$hashedPass = password_hash($password, PASSWORD_DEFAULT);		//hashing pass, this is what will be saved. PASSWORD_DEFAULT is the algorithm being used
		
		$statement = mysqli_stmt_init($connection);
		
		if(mysqli_stmt_prepare($statement, "INSERT INTO users (user, hash) VALUES(?,?)")){
			mysqli_stmt_bind_param($statement, "ss", $newUserName, $hashedPass);
			$exec = mysqli_stmt_execute($statement);
			mysqli_stmt_close($statement);
			
			if($exec){
				return true;
			}
			else{
				print "<script>console.log('statement prepare failed')</script>";
				return false;
			}
			
		}
		else{
			print "<script>console.log('statement prepare failed')</script>";
			return false;
		}
		
		//GOOD OLD QUERY BELOW. dont use it
		/*if (mysqli_query($connection, "INSERT INTO users (user, hash) VALUES('".$newUserName."','".$hashedPass."')")){ 
			return true;
		}
		else{
			return false;
		}*/
		
	}
	else{
		print "<script>console.log('user already exists')</script>";
		return false;
	}
}



/*
tries to connect to server, return false if not possible
you need to do this first of all
*/
function connectToServer($user,$pass,$server,$db){
	global $connection;
	$connection = mysqli_connect($server,$user,$pass,$db);
	mysqli_set_charset($connection,"utf8");	//sets charset to utf-8. databases can (and should) be encoded as utf-8-general-ci
	
	if($connection){
		return true;
	}
	else{
		die("connection to db failed: ".mysqli_connect_error());
		return false;
	}
}

/*
checks if input exists (variable name) from filter_input, with method POST, like filter_input(INPUT_POST, "varName");
returns false if no input, and the variable value if it exists
*/
function checkInput($input){
		if(filter_input(INPUT_POST, $input)){
			return filter_input(INPUT_POST, $input);
		}
		else{
			return false;
		}
}


/*
does a query to server, for example: SELECT * FROM tableName, INSERT INTO tableName (col1,col2) VALUES (val1,val2)
returns false if fails, or the result if successful
*/
function query($queryString){
	global $connection;
	return mysqli_query($connection,$queryString);
}

/*
Returning all fields in table
false if failure
*/
function getAllData($table){
	global $connection;	//using global var
	
	$sql = "SELECT * FROM ".$table;
	return mysqli_query($connection,$sql);
}


/*
checks length of string, returns true if larger or equal to $size
*/
function checkSize($size, $string){
	if (strlen($string) >= $size){
		return true;
	}
	else{
		return false;
	}
}

/*
Checks if string data contains illegal chars, returns false if so. should be used on usernames
you might want to trim whitespaces from certain non-important fields first
*/
function checkIfContainsIllegals($stringData){
	
	global $connection;
	
	if($stringData != strip_tags($stringData)){		//angle brackets
		return true;
	}
	
	if ($stringData != htmlspecialchars($stringData)){	//html code
		return true;
	}
	
	if($stringData != trim($stringData)){		//if whitespace
		return true;
	}
	
	if($stringData != stripslashes($stringData)){		//slash
		return true;
	}
	
	if($stringData != mysqli_real_escape_string($connection,$stringData)){		//anti sql injection
		return true;
	}
	
	return false;
}

?>