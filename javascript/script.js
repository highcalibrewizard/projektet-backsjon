$(document).ready(function(){					//loading in menu
	$("#meny").load("meny.html", function(){
		
		$("#news").click(function(){		//when loaded in menu, add click listeners to menu items
			document.location.href = "nyheter.php";
		});
		
		$("#gallery").click(function(){
			document.location.href = "galleri.php";
		});
		
		$("#about").click(function(){
			document.location.href = "index.php";
		});
		
		$("#backsjon").click(function(){
			document.location.href="backsjon.php";
		});
	});
	

	
	$("#footContent").load("foot.html", function(){	//loading footer. adding clicklistener as second arg, since we need to wait until it has loaded
		$("#loginHider").click(function(){
			$("#login").slideToggle();
			$("#loginHider").toggle();	//hiding button
			
			$("#login").submit(function(event){	//preventing default behaviour until form has been checked, THEN submitting
				
				
				
				if($("#pass").val().length > 0 && $("#user").val().length >0){
					//$("#login").submit();		//this is run by default after all submit code has been run
					console.log("stort nog");
				}
				else{
					event.preventDefault();		//prevent needs to be here
					$("#status").html("Var god fyll i båda fälten");
					console.log("inte stort nog");
				}
				
			});
		});
	});	
	
	$("#logout").click(function(){
		logOut();
	});
	
	loadFileUpload();
	loadNewsUpload();
	
});

//Hding login field when logged in
function hideLogin(){
	$("#footContent").hide();
	$("#logout").show();
}

//logging out and showing logged out message if successful
function logOut(){
	$.ajax({
		url: "logout.php"
	}).done(function(){
		statusChangeLogOut();
	});
}

function showFileForm(){
	$("#fileuploadDiv").show();
	$("#fileFormStatus").show();
	
}

function showNewsForm(){
	$("#newsuploadDiv").show();
	$("#newsFormStatus").show();
}


function loadNewsUpload(){
	$("#newsuploadDiv").load("newsupload.html", function(){
		//$("#fileuploadDiv").show();
		$("#newsuploadbtn").click(function(event){
			event.preventDefault();
			if(checkNewsFormValues())
				uploadNews();
		});
	});
	

}

function uploadNews(){
	$("#newsForm").submit();
}


function loadFileUpload(){
	$("#fileuploadDiv").load("fileupload.html", function(){
		//$("#fileuploadDiv").show();
		$("#fileuploadbtn").click(function(event){
			event.preventDefault();
			if(checkFileFormValues())
				uploadFile();
		});
	});
	

}

//hiding relevant stuff when logging out
function statusChangeLogOut(){
	
$("#status").html("Utloggad");	
$("#logout").hide();
$("#fileuploadDiv").hide();
$("#fileFormStatus").hide();
$("#newsuploadDiv").hide();
$("#newsFormStatus").hide();
$("#footContent").show();
//document.location.href="index.php";
}

function checkFileFormValues(){
	if (!$("#file").val() || !$("#text").val()){
		$("#fileFormStatus").html("Ange text och fil");
		return false;
	}
	return true;
}


function checkNewsFormValues(){
	if (!$("#title").val() || !$("#textnews").val()){
		$("#newsFormStatus").html("Ange text och titel");
		return false;
	}
	return true;
}

function fileUploadMess(message){
	$("#fileFormStatus").html(message);
}

function newsUploadMess(message){
	
	$("#newsFormStatus").html(message);
}

//TODO make this function
function uploadFile(){
	$("#fileForm").submit();
}

function statusChangeLogIn(user){
	
	$("#status").html("Inloggad som " +user);
}

function statusChangeFail(){
	$("#status").html("Inloggning misslyckades");
}